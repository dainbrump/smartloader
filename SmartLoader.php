<?php
/*!
 * SmartLoader Class
 * 
 * Creates an instance of the SmartLoader autoloader class. This class takes 
 * several configuration settings in the form of an associative array and 
 * recursively searches the library directory specified for files that have file 
 * extensions specified in the 'valid_extensions' configuration setting that do 
 * not reside in directories matching those specified in the 'exclude_directories' 
 * and adds them to an associative array where the key is the filename (minus 
 * extension) and the value is the full path to the file. This requires the file 
 * to be named exactly (case sensitive) the same as the primary class it contains.
 * 
 * Example: 'TestClass' would reside in a file named 'TestClass.class.php'. 
 *          Attempting to load 'TestClass' from a file named 'Testclass.class.php' 
 *          or 'testclass.class.php' will result in an error instantiating the 
 *          class.
 */
class SmartLoader {
	private $configuration = array(); ///<- Stores SmartLoader configuration

	/*!
	 * Private constructor. Instantiation should be done through the configure
	 * method.
	 */
	private function __construct () {
		return $this;
	}

	/*!
	 * Magic method to retrieve object values.
	 */
	public function __get ($key) {
		if (isset($this->configuration[$key])) {
			return $this->configuration[$key];
		}
		return null;
	}

	/*!
	 * Magic method to set object values.
	 */
	public function __set ($key, $value) {
		$this->configuration[$key] = $value;
		return $value;
	}

	/*!
	 * Magic method to check for the existance of object values.
	 */
	public function __isset ($key) {
		return (isset($this->configuration[$key]));
	}

	/*!
	 * SmartLoader Configuration
	 * 
	 * @param array $config The settings for the SmartLoader class.
	 * @return object Properly configured and created SmartLoader object.
	 */
	public static function configure (array $config = array()) {
		$SL = new SmartLoader();
		$SL->valid_extensions    = (isset($config['valid_extensions'])) ? $config['valid_extensions'] : array('.inc.php','.class.php');
		$SL->exclude_directories = (isset($config['exclude_directories'])) ? $config['exclude_directories'] : array();
		$SL->class_list_storage  = (isset($config['class_list_storage'])) ? $config['class_list_storage'] : 'memory';
		$SL->class_list_output   = (isset($config['class_list_output'])) ? $config['class_list_output'] : null;
		$SL->library_directory   = (isset($config['library_directory'])) ? $config['library_directory'] : dirname(__FILE__);
		$SL->rebuild_class_list  = (isset($config['rebuild_class_list'])) ? $config['rebuild_class_list'] : true;
		return $SL;
	}

	/*!
	 * SmartLoader Class List Retrieval
	 * 
	 * Returns an array containing a list of classes and their associated file 
	 * path and filename.
	 * 
	 * @return array The generated or loaded class list.
	 */
	public function get_class_list () {
		$cached_class_list = array(); ///<- Preset the class list.

		/* Is the cached class list file based, does it have a file associated and can we include it? */
		if ($this->class_list_storage === 'file' && $this->class_list_output !== null) {
			file_exists($this->class_list_output) && include($this->class_list_output);
			/* File doesn't exist or there is nothing in it. */
			if (empty($cached_class_list) || $this->rebuild_class_list) {
				/* Generate a class list and store it to the specified file. */
				$cached_class_list = $this->generate_class_list();
				if (!$this->write_class_list($cached_class_list)) {
					trigger_error("Cannot write to file. Ensure that the directory exists and is writable.", E_USER_ERROR);
					die;
				}
			}
		} else {
			$cached_class_list = $this->generate_class_list();
		}
		return $cached_class_list;
	}

	/*!
	 * SmartLoader Class List File Writer
	 * 
	 * Writes the generated class list to the file specified in the configure 
	 * method if the storage method is set to file.
	 * 
	 * @param array $class_list_array The generated class list array.
	 * @return boolean True on successful write, False otherwise.
	 */
	private function write_class_list ($class_list_array) {
		if (@file_put_contents($this->class_list_output, '<?php $cached_class_list = ' . var_export($class_list_array, true) . ';')) {
			@chmod($this->class_list_output, 0666);
			return true;
		}
		return false;
	}

	/*!
	 * SmartLoader Class List Generator
	 * 
	 * Recursively reads directories starting includin the 'library_directory' 
	 * and all nested directories for valid class files and builds an array of
	 * class name to class file associations.
	 * 
	 * @return array Class to class file associative array.
	 */
	public function generate_class_list () {
		$class_list = array();
		$rdi = new RecursiveDirectoryIterator($this->library_directory, RecursiveDirectoryIterator::FOLLOW_SYMLINKS);
		$slf = new SmartLoaderFilter($rdi, $this->valid_extensions, $this->exclude_directories);
		$rii = new RecursiveIteratorIterator($slf);
		foreach ($rii as $filename => $info) {
			$class_list[str_replace($this->valid_extensions, '', basename($filename))] = $filename;
		}
		return $class_list;
	}
}

/*!
 * SmartLoaderFilter Iterative Filter
 * 
 * Validates if the current directory is a directory to be included in the
 * search for valid classes or if the current filename has any of the valid file 
 * extensions for inclusion.
 * 
 * @see http://php.net/manual/en/class.recursivefilteriterator.php
 */
class SmartLoaderFilter extends RecursiveFilterIterator {
	public $valid_extensions;
	public $excluded_directories;

	public function __construct(RecursiveIterator $iterator, $ext, $dirs) {
		$this->valid_extensions = $ext;
		$this->excluded_directories = $dirs;
		parent::__construct($iterator);
	}

	public function accept () {
		$file = $this->current();
		$is_file = $file->isFile();
		$filename = $file->getFilename();
		$valid_ext = false;
		foreach ($this->valid_extensions as $ext) {
			$valid_ext |= (bool)strpos($filename, $ext) !== false;
		}
		return ($is_file && $valid_ext) || (!$is_file&& !in_array($filename, $this->excluded_directories));
	}

	public function getChildren() {
		return new self($this->getInnerIterator()->getChildren(), $this->valid_extensions, $this->excluded_directories);
	}
}