<?php
/*!
 * Application root directory (full system path)
 * 
 * This is not required. It is only used here to make demonstration easier. You 
 * are free to use and change it if needed. Constants such as this really should 
 * be defined in your application bootstrap / configuration script.
 */
define('APPROOT', '/home/mark/projects/web/loc/smartloader/dev/');

/*******************************************************************************
 * Include the following lines in your index / bootstrap script.
 ******************************************************************************/
/*!
 * Registers the autoload method. The function registered can be anything as 
 * long as the function is globally available.
 */
spl_autoload_register('autoload');

/*!
 * Generic autoload function
 * 
 * This function can be modified to perform additional instructions on class
 * autoload. The function presented is the minimal instructions required to get 
 * the SmartLoader to work.
 * 
 * @param string $class_name The name of the class/interface/object to load.
 * @return boolean True on successful load, False otherwise.
 */
function autoload ($class_name) {
	include_once('SmartLoader.php');
	static $class_list;

	if (is_null($class_list) || !isset($class_list[$class_name])){
		/*!
		 * SmartLoader Configuration Array
		 * 
		 * This array contains settings for the SmartLoader object. These settings 
		 * define:
		 *     1. What file extensions are valid for autoloading
		 *     2. Where to begin looking for those files
		 *     3. What directories should not be "included" for autoloading
		 *     4. Whether to cache autoloadable files
		 *     5. Where the autoloadable cache list should reside
		 * 
		 * This configuration array should be set prior to executing initializing 
		 * the SmartLoader object.
		 */
		$SmartLoaderConfiguration = array (
			/* What extensions are valid classes? */
			'valid_extensions'    => array('.class.php', '.interface.php'),
			/* What directory names should be ignored? */
			'exclude_directories' => array('.svn', 'unit_tests'),
			/* Class list storage method / referenced? Options: 'file' or 'memory'*/
			'class_list_storage'  => 'memory',
			/* If storage method == 'file', where? Else, set to null */
			//'class_list_output'   => APPROOT . 'cache/cached_class_list.php',
			'class_list_output'   => null,
			/* Starting path to search for classes. */
			'library_directory'   => APPROOT . 'demo/',
			/* Rebuild the class list if the class isn't found. Defaults to true if this isn't set here. */
			'rebuild_class_list'  => true
		);
		/* Create a new SmartLoader instance. */
		$sl = SmartLoader::configure($SmartLoaderConfiguration);
		/* Get our class list. */
		$class_list = $sl->get_class_list();
	}

	/* Does the called class exist? Require it. Otherwise die. */
	if (!is_null($class_list) && (isset($class_list[$class_name]) || file_exists($class_list[$class_name]))) {
		require($class_list[$class_name]);
		return true;
	}
	return false;
}